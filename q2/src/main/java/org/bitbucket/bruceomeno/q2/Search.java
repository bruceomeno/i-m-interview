package org.bitbucket.bruceomeno.q2;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

public class Search {

    public int doSearch(List<?> collection, Object value) {
        AtomicReference<Integer> res = new AtomicReference<>(-1);
        collection.forEach((Consumer<Object>) object -> {
            if (object.getClass() == value.getClass() && object == value) {
                res.set(collection.indexOf(object));
            }
        });
        return res.get();
    }
}
