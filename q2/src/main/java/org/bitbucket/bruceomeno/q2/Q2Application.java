package org.bitbucket.bruceomeno.q2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Q2Application {
    public static void main(String[] args) {
        System.out.println("Question 2");
        Search search = new Search();
        List<Integer> values = new ArrayList<>();
        values.add(1);
        values.add(3);
        values.add(2);
        values.add(0);
        System.out.println("Array => " + Arrays.toString(values.toArray()));
        int v = search.doSearch(values, 2);
        System.out.println("Index for 2 => " + v);
        int x = search.doSearch(values, 13);
        System.out.println("Index for 13 => " + x);
    }
}
