# Read Me
## I&M Interview Tasks

All tasks are in Java Language.
### Question one (q1 module)

Question 1 has two classes:
Q1Application class - Prime number solution 

Q1BandCApplication class - Memoization solution

### Question two (q2 module)

Q2Application class - Main class for such function solution
### Question three(q3 module)

Q3Application class - Web app consuming SWAPI API into table
### Getting started
**- Install libraries**

    Run the following command to install libraries from maven
    `mvn install -e`

**- Build packages**

    Build jar files for the various soltions provided.
    q1 module - Question one
    q2 module - Question two
    q3 module - Question three
    
    `mvn package -e`
    
    Each jar file is found target directory within the module's directory.
    
**NB:-** For questions one and two run the main classes.


