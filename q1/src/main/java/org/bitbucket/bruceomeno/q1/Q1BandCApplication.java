package org.bitbucket.bruceomeno.q1;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class Q1BandCApplication {

    //  isPrime function
    Function<Integer, Boolean> isPrime = number -> {
        // Check if number is greater than 1
        if (number <= 1) {
            return false;
        }

        // Check if number is from 2 onwards and if remainder is 0
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    };

    // Generic method that wraps a function into another function that will behave the same as input one, but checks if the result is already computed.
    public static <x, y> Function<x, y> memoize(Function<x, y> fn) {
        Map<x, y> result = new ConcurrentHashMap<>();
        return (a) -> result.computeIfAbsent(a, fn);
    }

    // Wrap the isPrime function
    Function<Integer, Boolean> memoizedIsPrime = memoize(isPrime);


    public static void main(String[] args) {
        System.out.println("Question 1 b and c: Memoization");
        Q1BandCApplication memoization = new Q1BandCApplication();
        // Call using the new memoizedIsPrime function.
        System.out.println(memoization.memoizedIsPrime.apply(23));

    }
}
