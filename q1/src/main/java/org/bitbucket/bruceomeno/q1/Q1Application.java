package org.bitbucket.bruceomeno.q1;

public class Q1Application {
    // Check if a number is prime
    public static boolean isPrime(int number) {
        // Check if number is greater than 1
        if (number <= 1) {
            return false;
        }

        // Check if number is from 2 onwards and if remainder is 0
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("Question 1: Prime Number");
        if (isPrime(23)) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }

    }
}
