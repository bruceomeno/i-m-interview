package org.bitbucket.bruceomeno.q3.utils;

import org.bitbucket.bruceomeno.q3.model.Actor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class Utils {

    private Logger log = LoggerFactory.getLogger(Utils.class);

    public String getErrorMessage(String errorType) {
        switch (errorType) {
            case "ia":
                return "Invalid actor id provided";
            case "na":
                return "Sorry, no actors found";
            default:
                return "Error encountered";
        }
    }

    public boolean checkFavorite(List<Actor> likedActors, Actor actor) {
        AtomicBoolean liked = new AtomicBoolean(false);
        likedActors.forEach(a -> {
            if (a.getName().equals(actor.getName())) {
                liked.set(true);
            }
        });
        return liked.get();
    }
}
