package org.bitbucket.bruceomeno.q3.controller;

import com.google.gson.Gson;
import org.bitbucket.bruceomeno.q3.model.Actor;
import org.bitbucket.bruceomeno.q3.model.People;
import org.bitbucket.bruceomeno.q3.store.model.FavoriteActorModel;
import org.bitbucket.bruceomeno.q3.store.repo.FavoriteActorRepository;
import org.bitbucket.bruceomeno.q3.utils.HttpUtils;
import org.bitbucket.bruceomeno.q3.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ActorController {

    private Logger log = LoggerFactory.getLogger(ActorController.class);

    @Autowired
    private HttpUtils httpUtils;

    @Autowired
    private Utils utils;

    @Autowired
    private FavoriteActorRepository favoriteActorRepository;

    private Gson gson = new Gson();

    @GetMapping(path = {"/", "/actors"})
    public String actors(
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "e", required = false) boolean error,
            @RequestParam(value = "type", required = false) String errorType,
            Model model
    ) {
        page = page != null && page > 0 ? page : 1;
        People people = httpUtils.getActors(page);
        if (people != null && people.getActors().size() > 0) {
            model.addAttribute("people", people);
        } else {
            if (!error) {
                error = true;
                errorType = "na";
            }
        }
        model.addAttribute("page", page);
        List<Actor> likedActors = new ArrayList<>();
        favoriteActorRepository.findAll().forEach(favoriteActor -> likedActors.add(gson.fromJson(favoriteActor.getActor(), Actor.class)));
        model.addAttribute("likedActors", likedActors);
        model.addAttribute("error", error && errorType != null ? utils.getErrorMessage(errorType) : null);
        model.addAttribute("utils", utils);
        return "actors";
    }

    @GetMapping(path = {"/actors/{id}/"})
    public String actorDetails(
            @PathVariable(name = "id") Long id,
            Model model
    ) {
        if (id != null && id > 0) {
            Actor actor = httpUtils.getActorDetails(id);
            if (actor != null) {
                model.addAttribute("actor", actor);
                return "actorDetails";
            } else {
                return "redirect:/actors?e=" + true + "&type=ia";
            }
        } else {
            return "redirect:/actors?e=" + true + "&type=ia";
        }
    }

    @GetMapping(path = {"/actors/like/{id}"})
    public String likeActor(
            @PathVariable(name = "id") Long id,
            @RequestParam(name = "page", required = false) Integer page
    ) {
        if (id != null && id > 0) {
            Actor actor = httpUtils.getActorDetails(id);
            if (actor != null) {
                favoriteActorRepository.findByActorId(id).ifPresentOrElse(favoriteActor -> {
                    favoriteActorRepository.delete(favoriteActor);
                }, () -> {
                    FavoriteActorModel favoriteActorModel = new FavoriteActorModel();
                    favoriteActorModel.setActorId(id);
                    favoriteActorModel.setActor(gson.toJson(actor));
                    favoriteActorRepository.save(favoriteActorModel);
                });
            }
        }
        page = page != null && page > 0 ? page : 1;
        return "redirect:/actors?page=" + page;
    }
}
