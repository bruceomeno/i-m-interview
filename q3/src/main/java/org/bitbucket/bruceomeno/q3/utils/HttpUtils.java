package org.bitbucket.bruceomeno.q3.utils;

import com.google.gson.Gson;
import org.bitbucket.bruceomeno.q3.model.Actor;
import org.bitbucket.bruceomeno.q3.model.People;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class HttpUtils {

    private String baseUrl = "https://swapi.co/api";
    private HttpClient client;

    private Logger log = LoggerFactory.getLogger(HttpUtils.class);

    public HttpUtils() {
        client = HttpClient.newBuilder()
                .followRedirects(HttpClient.Redirect.ALWAYS)
                .connectTimeout(Duration.of(60, ChronoUnit.SECONDS))
                .build();
    }

    public People getActors(int page) {
        AtomicReference<People> people = new AtomicReference<>(null);
        try {
            AtomicInteger p = new AtomicInteger(page > 0 ? page : 1);
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(baseUrl + "/people?format=json&page=" + p.get()))
                    .build();
            client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                    .thenApplyAsync(response -> {
                        try {
                            if (response.statusCode() == 200 && response.body() != null && !response.body().isEmpty()) {
                                people.set(new Gson().fromJson(response.body(), People.class));
                            }
                        } catch (Exception e) {
                            log.error("Get Actors Response Error - " + e.getMessage(), e);
                        }
                        return response;
                    })
                    .thenAccept(u -> log.info("Get People Response Page - " + p.get() + " Status - " + u.statusCode() + " Response - " + u.body()))
                    .join();
        } catch (Exception e) {
            log.error("Get Actors Error - " + e.getMessage(), e);
        }
        return people.get();
    }

    public Actor getActorDetails(long id) {
        AtomicReference<Actor> actor = new AtomicReference<>(null);
        try {
            if (id > 0) {
                HttpRequest request = HttpRequest.newBuilder()
                        .uri(URI.create(baseUrl + "/people/" + id))
                        .build();
                client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                        .thenApplyAsync(response -> {
                            try {
                                if (response.statusCode() == 200 && response.body() != null && !response.body().isEmpty()) {
                                    actor.set(new Gson().fromJson(response.body(), Actor.class));
                                }
                            } catch (Exception e) {
                                log.error("Get Actor Details Response Error - " + e.getMessage(), e);
                            }
                            return response;
                        })
                        .thenAccept(u -> log.info("Get Actor Details Response Id - " + id + " Status - " + u.statusCode() + " Response - " + u.body()))
                        .join();
            }
        } catch (Exception e) {
            log.error("Get Actor Details Error - " + e.getMessage(), e);
        }
        return actor.get();
    }

    /* methods for planets, films, species, vehicles, starships & actor details(people/id)*/
}
