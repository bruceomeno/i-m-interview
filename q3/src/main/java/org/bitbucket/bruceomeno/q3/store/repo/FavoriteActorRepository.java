package org.bitbucket.bruceomeno.q3.store.repo;

import org.bitbucket.bruceomeno.q3.store.model.FavoriteActorModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FavoriteActorRepository extends CrudRepository<FavoriteActorModel, Long> {
    Optional<FavoriteActorModel> findByActorId(Long id);
}
