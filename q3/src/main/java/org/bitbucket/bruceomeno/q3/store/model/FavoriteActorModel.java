package org.bitbucket.bruceomeno.q3.store.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class FavoriteActorModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true, length = 4200)
    private Long actorId;

    @Column(length = 4200)
    private String actor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActorId() {
        return actorId;
    }

    public void setActorId(Long actorId) {
        this.actorId = actorId;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    @Override
    public String toString() {
        return "FavoriteActorModel{" +
                "id=" + id +
                ", actorId=" + actorId +
                ", actor=" + actor +
                '}';
    }
}
